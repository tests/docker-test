#!/bin/bash

set -eEu

trap 'echo ; echo sdk-docker-test: FAILED' ERR

. /etc/os-release

docker run --rm registry.gitlab.apertis.org/infrastructure/apertis-docker-images/${VERSION_ID}-testcases-builder echo sdk-docker-test: PASSED
